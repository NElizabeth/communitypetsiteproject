var boot = function(game){};
  
boot.prototype = {
	preload: function(){
        console.log("Booting up Phaser Game");
        this.game.load.image("start_btn",this.game.global.assets+"/explore_btn.png");
	},
  	create: function(){
		var startButton = this.game.add.button(400,300,"start_btn",this.loadGame,this);
		startButton.anchor.setTo(0.5,0.5);
	},
    loadGame: function() {
        this.game.state.start("Load");
    }
}