<!DOCTYPE html>
<html>
    <head>
        <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css">
        @yield('head')
    </head>
    <body>
        <div class="wrapper">
            <header>
                <img src="{{ asset('images/logo.png') }}" alt="Logo" id="logo"></img>
                <div id="player-information">
                    <div>
                        <img src="{{ asset('images/petexample100x100.png') }}" id="pet"></img>
                        <div id="avatar">
                            <div id="username">NElizabeth</div>
                        </div>
                    </div>
                    <div>
                        <span id="currency">650</span><span id="currency-icon">C</span>
                        <img src="" alt="" class="icons"></img>
                        <img src="" alt="" class="icons"></img>
                        <img src="" alt="" class="icons"></img>
                        <div id="player-location">Grayden Hollow</div>
                    </div>
                </div>
            </header>
            <nav>
                <ul>
                    <li>Link 1</li>
                    <li>Link 2</li>
                    <li>Link 3</li>
                    <li>Link 4</li>
                    <li><a href="{{ url('/explore') }}">Explore</a></li>
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                    @endif
                </ul>
            </nav>
            <div id="content" style="position:relative;">
                @yield('content')
            </div>
            <div class="push"></div>
        </div>
        <div class="footer">
            <div id="footer-container" style="padding:10px;">
                Copyright &copy; 2016
            </div>
        </div>
    </body>
</html>
