@extends('master')

@section('head')
    <script type="text/javascript" src="{{ asset('js/phaser.min.js') }}"></script>
    
    <!-- Import javascript files that define each state -->
    <script type="text/javascript" src="{{ asset('js/boot.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/explore.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/load.js') }}"></script>
@endsection

@section('content')
<div>
    This is the exploration page.
    <div id="explore-phaser-canvas"></div>

    <script type="text/javascript">
    window.onload = function() {
    
        // Create game
        var game = new Phaser.Game(800, 600, Phaser.AUTO, 'explore-phaser-canvas');
        
        // Declare global variables
        game.global = {};
        game.global.assets = "{{ URL::to('/js/assets') }}";
        
        // Declare states
        game.state.add("Boot",boot);
        game.state.add("Load",load);
        game.state.add("Explore",explore);
        
        game.state.start("Boot");
        
    };

    </script>
    
</div>
@endsection
