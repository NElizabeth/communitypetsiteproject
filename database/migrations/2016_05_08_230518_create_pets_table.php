<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('species');
            $table->string('image');
            $table->string('sprite');
            $table->string('theme')->nullable();
            $table->string('climate');
            $table->string('type');
            $table->text('description');
            $table->boolean('genderless')->default(false);
            $table->boolean('can_jump')->default(false);
            $table->boolean('can_sprint')->default(false);
            $table->boolean('can_climb')->default(false);
            $table->boolean('can_dig')->default(false);
            $table->boolean('can_swim')->default(false);
            $table->boolean('can_fly')->default(false);
            $table->integer('base_energy');
            $table->integer('base_health');
            $table->integer('base_attack');
            $table->integer('base_defense');
            $table->integer('base_speed');
            $table->integer('base_regen');
            $table->integer('nutrition_grains');
            $table->integer('nutrition_vegetables');
            $table->integer('nutrition_fruits');
            $table->integer('nutrition_protein');
            $table->integer('nutrition_dairy');
            $table->integer('nutrition_sweets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pets');
    }
}
